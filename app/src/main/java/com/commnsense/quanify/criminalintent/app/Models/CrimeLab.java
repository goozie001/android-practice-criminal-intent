package com.commnsense.quanify.criminalintent.app.Models;

import android.content.Context;
import android.util.Log;

import com.commnsense.quanify.criminalintent.app.Storage.CriminalIntentJSONSerializer;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Dylan on 5/15/2014.
 */
public class CrimeLab {
    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";

    private CriminalIntentJSONSerializer mSerializer;

    private static CrimeLab sCrimeLab;
    private ArrayList<Crime> crimeList;
    private Context mAppContext;

    private CrimeLab(Context appContext) {
        mAppContext = appContext;
        mSerializer = new CriminalIntentJSONSerializer(mAppContext, FILENAME);

        try {
            crimeList = mSerializer.loadCrimes();
        } catch (Exception e) {
            crimeList = new ArrayList<Crime>();
            Log.e(TAG, "Error loading crimes: ", e);
        }
    }

    public static CrimeLab get(Context c) {
        if (sCrimeLab == null) {
            sCrimeLab = new CrimeLab(c.getApplicationContext());
        }
        return sCrimeLab;
    }

    public void addCrime(Crime c) {
        crimeList.add(c);
    }

    public void deleteCrime(Crime c) {
        crimeList.remove(c);
    }

    public ArrayList<Crime> getCrimes() {
        return crimeList;
    }

    public Crime getCrime(UUID id) {
        for (Crime c : crimeList) {
            if (c.getmId().equals(id)) return c;
        }
        return null;
    }

    public boolean saveCrimes() {
        try {
            mSerializer.saveCrimes(crimeList);
            Log.d(TAG, "crimes saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving crimes: ", e);
            return false;
        }
    }
}
