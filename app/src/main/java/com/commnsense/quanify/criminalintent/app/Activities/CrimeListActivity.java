package com.commnsense.quanify.criminalintent.app.Activities;

import android.support.v4.app.Fragment;

import com.commnsense.quanify.criminalintent.app.Fragments.CrimeListFragment;

/**
 * Created by Dylan on 5/16/2014.
 */
public class CrimeListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
