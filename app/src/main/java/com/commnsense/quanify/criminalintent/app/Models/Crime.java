package com.commnsense.quanify.criminalintent.app.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Dylan on 5/14/2014.
 */
public class Crime {
    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_SOLVED = "solved";
    private static final String JSON_DATE = "date";

    private UUID mId;
    private String mTitle;
    private Date mDate;
    private SimpleDateFormat mDateFormat;
    private SimpleDateFormat mTimeFormat;
    private boolean mSolved;
    private String fullDate;


    public Crime() {
        mId = UUID.randomUUID();
        mDate = new Date();
        mDateFormat = new SimpleDateFormat("EEEE, M/d/yyyy");
        mDateFormat.format(mDate);
        mTimeFormat = new SimpleDateFormat("h:mm a");
        fullDate = mTimeFormat.format(mDate) + ", " + mDateFormat.format(mDate);
    }

    public Crime(JSONObject json) throws JSONException {
        mId = UUID.fromString(json.getString(JSON_ID));
        if (json.has(JSON_TITLE)) {
            mTitle = json.getString(JSON_TITLE);
        }
        mSolved = json.getBoolean(JSON_SOLVED);
        mDate = new Date(json.getLong(JSON_DATE));
        mDateFormat = new SimpleDateFormat("EEE, M/d/yyyy");
        mTimeFormat = new SimpleDateFormat("h:mm a");
        fullDate = mTimeFormat.format(mDate) + ", " + mDateFormat.format(mDate);

    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mId.toString());
        json.put(JSON_TITLE, mTitle);
        json.put(JSON_SOLVED, mSolved);
        json.put(JSON_DATE, mDate.getTime());
        return json;
    }

    public String getFullDate() {
        return mTimeFormat.format(mDate) + ", " + mDateFormat.format(mDate);
    }

    public SimpleDateFormat getmTimeFormat() {
        return mTimeFormat;

    }

    public SimpleDateFormat getmDateFormat() {
        return mDateFormat;
    }

    public String getmTitle() {
        return mTitle;
    }

    public UUID getmId() {
        return mId;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public boolean ismSolved() {
        return mSolved;
    }

    public void setmSolved(boolean mSolved) {
        this.mSolved = mSolved;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
